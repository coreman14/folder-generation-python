# SimpleFolderStructure - Save and create directories from a file

SimpleFolderStructure allows you to save and share directory structure's from a txt file. It' allows 
* Save file contents as base64, then have the contents copied when created
* Choose which files using regex or types
* Exclude files and folders using regex or types
* Custom indent size and characters
* Custom prefix's for each type of files

# Installation
Install through pip:

    python3 -m pip install SimpleFolderStructure
    
# Documentation

### Saving structure's 
To use SimpleFolderStructure, navigate to the directory you want to copy and start the script using:

    folderstructure save "Outputfile.txt"
    
This will create a script that will output your current directory in a format like so:

```
#Outputfile.txt
: afileinthestartdirectory.bat
: grandmascookies.docx
* DirectoryBelowMain
    : afileinthatdirectory.py
    : anotherone.txt
* AnotherDirectory
    : randomfile.js
```

Using -intype or -inregex, any matches content will be encoded to base64 and placed on the same line. When they are created, they will share the same content:

```
folderstructure save "Outputfile.txt" -intype txt -inrex ".*that.*"
#Outputfile.txt
: afileinthestartdirectory.bat
: grandmascookies.docx
* DirectoryBelowMain
    ? afileinthatdirectory.py|cHJpbnQoIkEgYmFzZSA2NCBlbmNvZGVkIHB5dGhvbiBmaWxlISIp
    ? anotherone.txt|RHVjaw==
* AnotherDirectory
    : randomfile.js
```

Adding -extype or -exrex, any matches will be excluded from the output. This will nullify if the file was included within the -intype or -inrex:
```
folderstructure save "Outputfile.txt" -extype bat docx
#Outputfile.txt
* DirectoryBelowMain
    : afileinthatdirectory.py
    : anotherone.txt
* AnotherDirectory
    : randomfile.js
```
Adding -exfold will remove folders that match the regex. This will remove entire path's meaning any files after will be removed too:
```
folderstructure save "Outputfile.txt" -exfold .*Another.*
#Outputfile.txt
: afileinthestartdirectory.bat
: grandmascookies.docx
* DirectoryBelowMain
    : afileinthatdirectory.py
    : anotherone.txt
```

#### NOTE: Excludes are done before includes, so if a file is found to be excluded it will not add it even if included by type or regex
### Creating structures from files
To create a structure, input create and the file to use to make it:

    folderstructure create "Outputfile.txt"

This will create all files, not overriding existing ones.

    Created 3 files, 1 file already existed.

### Editing output style
Using -s and -c you can make the characters and size of the indent different to create a different look

```
folderstructure save "Outputfile.txt" -s 3 -c *
#3 * per indent
#Outputfile.txt
: afileinthestartdirectory.bat
: grandmascookies.docx
* DirectoryBelowMain
***: afileinthatdirectory.py
***: anotherone.txt|RHVjaw==
* AnotherDirectory
***: randomfile.js
```

Character can be any length and will repeat the number of times given to size
```
folderstructure save "Outputfile.txt" -s 2 -c #@
# 2 sets of #@ per indent
#Outputfile.txt
: afileinthestartdirectory.bat
: grandmascookies.docx
* DirectoryBelowMain
#@#@: afileinthatdirectory.py
#@#@: anotherone.txt
* AnotherDirectory
#@#@: randomfile.js
```

### Prefix's
While prefix customization exists, the characters used for a prefix are characters that are not allowed in windows files. Please be careful when changing them, as changing them could result in error's. Prefix's checks are done in alphabetical order, so if a base64 prefix is in a name, it will find it first

EX:
Setting the BASE64 prefix to yB would make '* DirectoryBelowMain' a BASE64 file. It will not cause an error as list slicing cannot produce an error
    
# TODO's
* [ ] Config file to avoid long command line arguments
- [ ] Input for directory or file names. 
- Directory: (Name for directory) ask's for a name in the cmdline before creating a file
- File: (Name for main script|py) will create a file with input from the cmdline with the extension py
* [ ] Base64 files to let user's enter text from the cmdline. 
* This could be useful for package name and consolescript name when creating setup.py.
- [ ] Allow for creation of variables while creating files to avoid multiple repeated entries


# FAQ

### Why can't I run this on <3.10?

Cause I used the union operator.
If you want to run this on 3.9 and lower, check out the github for the files that end with "_35". I may try to keep those updated
### Why use this over something like a zip file?
With a zip file, you have to give everything in the state it was created, meaning if you trying to give someone a blank starter pack, like for bundling a python app, you have to either edit the files your putting in or create new blank ones somewhere else.

With this app you can give user's a template with only information you want filled in, with all other files being blank. I made this app because i saw this on a tutorial:

```
MyTool
    - app
        - __init__.py
        - __main__.py
        - application.py
    - my_tool.py
    - setup.py
    - requirements.txt
    - README.md
    - LICENSE
    - MANIFEST.in
```

And I had to go through and create all those files by hand and copy what was given to me. So I set out to make a tool that could do that.

With this tool, this could easily be replaced with:

```
* MyTool
    * app
        : __init__.py
        : __main__.py
        : application.py
    : my_tool.py
    ? setup.py|{Code for setup file}
    - requirements.txt
    - README.md
    - LICENSE
    - MANIFEST.in
```

While this isn't much, any more hardcoded code that a user could need for a project could also be given. And with my next feature's being input from a Command line when creating files and folders, I think that my tool could hold a very niche place in peoples setup routines

